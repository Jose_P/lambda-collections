package org.paumard.collection.spliterator;

import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class SingletonSpliterator<E> implements Spliterator<E> {

    private final Supplier<E> supplier;
    private boolean done = false;

    public SingletonSpliterator(Supplier<E> supplier) {
        this.supplier = supplier;
    }

    @Override
    public boolean tryAdvance(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        if (done)
            return false;
        done = true;
        action.accept(supplier.get());
        return true;
    }

    @Override
    public Spliterator<E> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return done ? 0 : 1;
    }

    @Override
    public int characteristics() {
        return Spliterator.CONCURRENT |
                Spliterator.IMMUTABLE | Spliterator.NONNULL |
                Spliterator.DISTINCT | Spliterator.SIZED | Spliterator.SORTED;
    }
}
