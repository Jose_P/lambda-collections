package org.paumard.collection.spliterator;

import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntFunction;

public class TwoElementsSpliterator<E> implements Spliterator<E> {

    private final IntFunction<E> function;
    private int index = 0;
	private int characteristics;

    public TwoElementsSpliterator(IntFunction<E> function, int characteristics) {
        this.function = function;
        this.characteristics = characteristics;
    }

    @Override
    public boolean tryAdvance(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        if (index == 2)
            return false;

        action.accept(function.apply(index++));
        return true;
    }

    @Override
    public Spliterator<E> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return 2 - index;
    }

    @Override
    public int characteristics() {
        return this.characteristics;
    }
}
