package org.paumard.map;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface KeyValueEntry<K, V> extends Map.Entry<K, V>, Function<Boolean, Object> {

    static <K, V> KeyValueEntry<K, V> of(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);
        return b -> b ? key : value;
    }

    @SuppressWarnings("unchecked")
	default K getKey() {
        return (K)apply(true);
    }

    @SuppressWarnings("unchecked")
    default V getValue() {
        return (V)apply(false);
    }

    default V setValue(V value) {
        throw new UnsupportedOperationException();
    }
}
