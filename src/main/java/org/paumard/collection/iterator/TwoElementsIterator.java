package org.paumard.collection.iterator;

import java.util.Iterator;
import java.util.function.IntFunction;

public class TwoElementsIterator<E> implements Iterator<E> {

    private int cursor = 0;

    private final IntFunction<E> function;

    public TwoElementsIterator(IntFunction<E> function) {
        this.function = function;
    }

    @Override
    public boolean hasNext() {
        return cursor < 2;
    }

    @Override
    public E next() {
        return function.apply(cursor++);
    }
}
