package org.paumard.collection.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

public class SingletonIterator<E> implements Iterator<E> {

    private final Supplier<E> supplier;
    private boolean done = false;

    public SingletonIterator(Supplier<E> supplier) {
        this.supplier = supplier;
    }

    @Override
    public boolean hasNext() {
        return !done;
    }

    @Override
    public E next() {
        Supplier<E> exceptionSupplier = () -> {throw new NoSuchElementException();};
        E result = done ? exceptionSupplier.get() : supplier.get();
        done = true;
        return result;
    }

}
